Source: qstylizer
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Julian Gilbey <jdg@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-inflection,
               python3-pbr,
               python3-pytest <!nocheck>,
               python3-pytest-mock <!nocheck>,
               python3-setuptools,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-sphinxcontrib.autoprogram <!nodoc>,
               python3-tinycss2
Standards-Version: 4.6.1
Homepage: https://github.com/blambright/qstylizer
Vcs-Git: https://salsa.debian.org/python-team/packages/qstylizer.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/qstylizer
Rules-Requires-Root: no

Package: python3-qstylizer
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-qstylizer-doc
Description: Python package for construction of PyQt/PySide stylesheets
 qstylizer allows for both the construction of new stylesheets and
 the easy modification of existing ones using idiomatic Python code.

Package: python-qstylizer-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Recommends: python3-qstylizer
Description: Python package for construction of PyQt/PySide stylesheets (documentation)
 qstylizer allows for both the construction of new stylesheets and
 the easy modification of existing ones using idiomatic Python code.
 .
 This package contains the qstylizer documentation in HTML format.
Build-Profiles: <!nodoc>
